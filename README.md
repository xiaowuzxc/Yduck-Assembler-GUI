# 大黄鸭汇编器GUI版
本工程服务于[大黄鸭处理器](https://gitee.com/xiaowuzxc/Yduck-processor)
### 介绍
大黄鸭增强版汇编器，依赖于Tk，具备~~友好的~~GUI界面。  
内含使用pyinstaller生成的Assembler.exe文件，无需安装python环境即可使用。  
~~猴子都会使用的汇编器~~  
[release版下载](https://gitee.com/xiaowuzxc/Yduck-Assembler-GUI/releases/v2.0)  
![界面](/img.png)  

### 使用说明
#### 脚本使用说明(win)
请安装python3.7及以上版本。  
双击run.bat打开GUI界面。  

理论上linux也可以运行，但是需要python支持tkinter  

#### exe版使用说明
打开Assembler.exe  
单击“打开文件”，选择汇编程序文件  
单击“汇编”，执行汇编任务，在当前目录输出obj.txt文件  

### exe生成(win)
请安装python3.7及以上版本，安装pyinstaller。  
双击pkg.bat，在dist目录下生成exe  
